import { AccountsTemplates } from 'meteor/useraccounts:core';
import { TAPi18n } from 'meteor/tap:i18n';
import { Session } from 'meteor/session';
import { Roles } from 'meteor/alanning:roles';

function getRoute(){

      if(Session.get('redirectAfterLogin'))
        return Session.get('redirectAfterLogin');
      else
        return '/'

}


const postSignUp = (userId, info)=>{
  console.log(userId)
  console.log('test');
    Roles.addUsersToRoles(userId, ['admin']);
}
AccountsTemplates.configure({
    showForgotPasswordLink: true,
    texts: {
        errors: {
            loginForbidden: TAPi18n.__('Błędny login lub hasło'),
            pwdMismatch: TAPi18n.__('Hasła nie są takie same!'),
        },
        title: {
            signIn: TAPi18n.__('Zaloguj się'),
            signUp: TAPi18n.__('Dołącz'),
        },
    },
    homeRoutePath: getRoute(),
    redirectTimeout: 4000,
    postSignUpHook: postSignUp,
    defaultTemplate: 'Auth_page',
    defaultLayout: 'App_body',
    defaultContentRegion: 'main',
    defaultLayoutRegions: {},
});
