import { FlowRouter } from 'meteor/kadira:flow-router';
import { BlazeLayout } from 'meteor/kadira:blaze-layout';
import { AccountsTemplates } from 'meteor/useraccounts:core';
import { Roles } from 'meteor/alanning:roles'
import { Session } from 'meteor/session';

import '../../ui/layout/app-body.js';
import '../../ui/views/messages/messages-list.js';
import '../../ui/views/projects/project-show.js';
import '../../ui/views/projects/project-list.js';
import '../../ui/views/messages/messages-list.js';
import '../../ui/views/messages/messages-box.js';
import '../../ui/views/notfound/not-found.js';
import '../../ui/accounts/accounts-templates.js';


/*Groups*/
const loggedIn = FlowRouter.group({
  triggersEnter: [
    () => {
      let routeer = '';
      if(!(Meteor.loggingIn() || Meteor.userId()))
      {
          routeer = FlowRouter.current();
          if(routeer.route.name !== 'signin')
              Session.set('redirectAfterLogin', routeer.path);
          FlowRouter.go('/signin');
      }
    },
  ]
})


const adminsGroup = loggedIn.group({
  prefix:'/admin',
  triggersEnter: [
    () => {
        if(!Roles.userIsInRole(Meteor.user(), ['admin']))
        {

        }
    }
  ],
})


loggedIn.route('/project/:_id',{
  name: 'Project.show',
  action(){
    BlazeLayout.render('App_body',{ main: 'Project_show'})
  }
});


loggedIn.route('/',{
  name: 'Project.list',
  action(){
    BlazeLayout.render('App_body',{ main: 'Project_list'})
  }
})


FlowRouter.notFound = {
  action(){
    BlazeLayout.render('Not_found');
  }
}




/* Accounts Routing */
AccountsTemplates.configureRoute('signIn', {
    name: 'signin',
    path: '/signin',
});
AccountsTemplates.configureRoute('signUp', {
    name: 'join',
    path: '/join',
});

AccountsTemplates.configureRoute('forgotPwd');

AccountsTemplates.configureRoute('resetPwd', {
    name: 'resetPwd',
    path: '/reset-password',
});
