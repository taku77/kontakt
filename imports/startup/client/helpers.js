import { Roles } from 'meteor/alanning:roles';
import { Template } from 'meteor/templating';
import { Projects } from '../../api/projects/projects.js';

Template.registerHelper("isAdmin", function(user){
  return Roles.userIsInRole(user,['admin']);
});

Template.registerHelper('getHour', function(date){
      return moment(date).format('H:mm');
  });
  Template.registerHelper('getDay', function(date){
        return moment(date).format('DD/MM/YYYY');;
    });
Template.registerHelper("isProjectActice", function(projectId){
  return Projects.findOne(projectId);
});
