import { Meteor } from 'meteor/meteor';
import { _ } from 'meteor/underscore';
import { ValidatedMethod } from 'meteor/mdg:validated-method';
import { SimpleSchema } from 'meteor/aldeed:simple-schema';
import { DDPRateLimiter } from 'meteor/ddp-rate-limiter'
import { Roles } from 'meteor/alanning:roles';

export const updateRole = new ValidatedMethod({
  name: 'user.updaterole',
  validate: new SimpleSchema({
    userId: {type: String},
    roles: {type: [String]}
  }).validator(),
  run({userId,roles}){
    if(!this.userId){
      throw new Meteor.Error('messages.onlyForLogged',
        'Dodawnaie wiadomości tylko dla zalogowanych')
    }

    if(!(Roles.getGroupsForUser(userId).length > 0))
    {
        Roles.setUserRoles(userId, roles);
    }

  }
})



const USERS_METHODS = _.pluck([
  updateRole,
],'name')


if(Meteor.isServer){
  DDPRateLimiter.addRule({
    name(name){
      return _.contains(USERS_METHODS,name);
    },
    connectionId(){ return true;},

  },5,1000);
}
