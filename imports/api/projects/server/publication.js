import { Meteor } from 'meteor/meteor';
import { Projects } from '../projects.js';
import { Roles } from 'meteor/alanning:roles'

Meteor.publish('projects.all', function projectsAll(){

  if(this.userId && Roles.userIsInRole(this.userId, ['admin']))
  {
    return Projects.find({}, {fields : Projects.publicFields});
  }
  else if(this.userId && Roles.userIsInRole(this.userId, ['client']))
      return Projects.find({clientsId: this.userId}, {fields: Projects.publicFields});
})
