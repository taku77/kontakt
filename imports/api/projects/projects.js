import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { SimpleSchema } from 'meteor/aldeed:simple-schema'
import { EasySearch } from 'meteor/easy:search';



export const Projects = new Mongo.Collection('Projects');
Projects.deny({
  insert() {return false;},
  update() { return true; },
  remove() { return true; },
})


Projects.schema = new SimpleSchema({
  name: {
    type: String,
    max: 100,
  },
  text:{
    type: String,
    optional: true
  },
  clientsId: {
    type: [String],
    optional: true,
  },
  siteUrl:{
    type: String,
    optional: true,
  },
  createdAt: {
    type: Date,
    optional: true,
    autoValue: () =>{

        return new Date();

    },
    denyUpdate: true,
  },
})

Projects.attachSchema(Projects.schema);
Projects.publicFields = {
  name: 1,
  text: 1,
  createdAt: 1,
  siteUrl: 1,
}


Projects.form = new SimpleSchema({
  name: {
    type: String
  },
  clientsId: {
    type: [String],
    optional: true,

  },
  siteUrl:{
    type: String,
    optional: true,
  },

  text:{
    type: String,
    optional: true
  },
});
