import { SimpleSchema } from 'meteor/aldeed:simple-schema';
import { Projects } from './projects.js';

Projects.form = new SimpleSchema({
  name: {
    type: String
  },
  clientsId: {
    type: [String],
    optional: true,
    autoform: {
      atFieldInput:{
        type: 'select-multiple',
        options: [
          {label:'test', value:'asdasd'},
          {label:'test2', value:'asdasd2'},

        ]
      }
    }
  },
  siteUrl:{
    type: String,
    optional: true,
  },
});
