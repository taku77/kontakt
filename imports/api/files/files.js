import { Mongo } from 'meteor/mongo';

import { SimpleSchema } from 'meteor/aldeed:simple-schema';



export const Files = new FS.Collection('files',{
  stores: [
    new FS.Store.FileSystem("files",{ path: process.env.PWD + 'upload/files'})
  ]
})



Files.allow({
  'insert': function(){
    return true;
  }
})
