import { EasySearch } from 'meteor/easy:search';
import { Messages } from './messages.js';

export const MessagesSearch = new EasySearch.Index({
  collection: Messages,
  fields: ['name'],
  name: 'messagesSearch',
  engine: new EasySearch.MongoDB({
      sort: () => {createdAt : -1},
      selector: function(searchObject, options, aggregation){
            let selector = this.defaultConfiguration().selector(searchObject, options, aggregation);
            let newSelector = {'$and' :[]}
            newSelector['$and'] = selector['$or']
            newSelector['$and'].push({'messageId' : {'$exists' : false}})

            return newSelector;
        }
  })
})
