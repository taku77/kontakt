import { Mongo } from 'meteor/mongo';
import { SimpleSchema } from 'meteor/aldeed:simple-schema';
import { Collection2 } from 'meteor/aldeed:collection2';



export const Messages = new Mongo.Collection('Messages');


Messages.deny({
  insert() {return true;},
  update() { return true; },
      remove() { return true; },
})


Messages.schema = new SimpleSchema({
  name: {
    type: String,
    max: 100,
  },
  text: {
    type: String,
    max: 1000,
  },
  isAccepted:{
    type: Boolean,
    defaultValue: false
  },
  userId: {
    type: String,
    optional: true,
    autoValue: ()=>{
      if(Meteor.userId())
        return Meteor.userId()
    }

  },
  projectId:{
    type: String,
    optional: true
  },
  messageId:{
    type: String,
    optional: true
  },
  createdAt:{
    type: Date,
    autoValue: function(){
      if (this.isInsert) {
        return new Date();
      } else if (this.isUpsert) {
        return {$setOnInsert: new Date()};
      } else {

      }
    },
    denyUpdate: true,
  },
  updatedAt: {
    type: Date,
    autoValue: function() {
      if (this.isUpdate) {
        return new Date();
      }
    },
    denyInsert: true,
    optional: true
  },
})


Messages.attachSchema(Messages.schema);


Messages.publicFields = {
  name: 1,
  text: 1,
  createdAt: 1,
  projectId: 1,
  isAccepted: 1,
  messageId: 1,
}

Messages.helpers({
  editableBy(userId){
    if(!this.userId){
      return true;
    }

    return this.userId === userId;
  }
})
