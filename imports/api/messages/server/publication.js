import { Meteor } from 'meteor/meteor';
import { Messages } from '../messages.js';

Meteor.publish('messages.all', function messagesAll(){
  return Messages.find({}, {fields : Messages.publicFields});
})
