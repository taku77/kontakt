import { Meteor } from 'meteor/meteor';
import { _ } from 'meteor/underscore';
import { ValidatedMethod } from 'meteor/mdg:validated-method';
import { SimpleSchema } from 'meteor/aldeed:simple-schema';
import { DDPRateLimiter } from 'meteor/ddp-rate-limiter'


import { Messages } from './messages.js';

export const insert = new ValidatedMethod({
  name: 'messages.insert',
  validate: new SimpleSchema({
    name: {type: String},
    text: {type: String},
    projectId: {type: String},
  }).validator(),
  run({name, text, projectId}){
    if(!this.userId){
      throw new Meteor.Error('messages.onlyForLogged',
        'Dodawnaie wiadomości tylko dla zalogowanych')
    }

    const message = {
      name,
      text,
      projectId
    }

    Messages.insert(message);
  }
})

export const insertToMessage = new ValidatedMethod({
  name: 'messages.inserttomessage',
  validate: new SimpleSchema({
    name: {type: String},
    text: {type: String},
    projectId: {type: String},
      messageId: {type: String},
  }).validator(),
  run({name, text, projectId,messageId}){
    if(!this.userId){
      throw new Meteor.Error('messages.onlyForLogged',
        'Dodawnaie wiadomości tylko dla zalogowanych')
    }

    const message = {
      name,
      text,
      projectId,
      messageId
    }

    Messages.insert(message);
  }
})

export const getMessagesForProject = new ValidatedMethod({
  name: 'messages.getmessagesforproject',
  validate: new SimpleSchema({
    projectId: {type: String}
  }).validator(),
  run({projectId})
  {
    if(!this.userId)
      throw new Meteor.Error('messages.onlyForLogged',
              'Dodawnaie wiadomości tylko dla zalogowanych');


    return Messages.find({$and :[{projectId : projectId},{messageId: false} ]},{sort: {createdAt: -1}});
  }
})
export const getMessagesForMessage = new ValidatedMethod({
  name: 'messages.getmessagesformessage',
  validate: new SimpleSchema({
    messageId: {type: String}
  }).validator(),
  run({messageId})
  {
    if(!this.userId)
      throw new Meteor.Error('messages.onlyForLogged',
              'Dodawnaie wiadomości tylko dla zalogowanych');


    return Messages.find({messageId : messageId},{sort: {createdAt: -1}});
  }
})


export const acceptMessage = new ValidatedMethod({
  name: 'messages.acceptmessage',
  validate: new SimpleSchema({
    messageId: {type :String},
  }).validator(),
  run({messageId}){
    if(!this.userId)
      throw new Meteor.Error('messages.onlyForLogged',
              'Dodawnaie wiadomości tylko dla zalogowanych');

    return Messages.update( messageId,{$set:{isAccepted: true}});
    }
})

const MESSAGES_METHODS = _.pluck([
  insert,
  getMessagesForProject,
],'name')


if(Meteor.isServer){
  DDPRateLimiter.addRule({
    name(name){
      return _.contains(MESSAGES_METHODS,name);
    },
    connectionId(){ return true;},

  },5,1000);
}
