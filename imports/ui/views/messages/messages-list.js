import './messages-list.html';
import { Template } from 'meteor/templating';
import { FlowRouter } from 'meteor/kadira:flow-router';
import {
  insert,
  getMessagesForProject }
  from '../../../api/messages/methods.js';
  import {
    Messages }
    from '../../../api/messages/messages.js';
import { MessagesSearch }   from '../../../api/messages/serach.js';
Template.Messages_list.helpers({
  messages(){
    return getMessagesForProject.call({
      projectId : FlowRouter.getParam('_id')
    })

  },
  messagesSearch(){
    return MessagesSearch;
  },
  searchAttr(){
        return {
            class: 'form-control'
        }
    },
});


Template.Messages_list.events({
  "submit .add-new-message": function(event, template){
    event.preventDefault();
    const $name = $(event.target).find('#message-title');
    const $text = $(event.target).find('#message-text');

    if(!$name.val() && !$text.val())
      return false;

    insert.call({
      name: $name.val(),
      text: $text.val(),
      projectId: FlowRouter.getParam('_id')
    },(error) => {
      if(error)
        console.log(error);
    })

    $name.val('');
    $text.val('');


  }
});
