import './messages-box.html';

import { Template } from 'meteor/templating';
import {
  acceptMessage,
  insertToMessage,
  getMessagesForMessage}
  from '../../../api/messages/methods.js';
Template.Messages_box.helpers({
  accepted(){
    console.log(this._id.substring(0,17));
    return this.isAccepted && 'accepted';
  },
  messages(){
    return getMessagesForMessage.call({
      messageId : this._id.substring(0,17)
    })

  },
});
Template.Messages_box.events({
  "click .accept-button": function(event, template){
        const messageId = this._id;
        acceptMessage.call({
          messageId: messageId
        });
  },
  "submit .add-message-to-message": function(event, template){
    event.preventDefault();
    const $name = $(event.target).find('#message-title');
    const $text = $(event.target).find('#message-text');

    if(!$name.val() && !$text.val())
      return false;

    insertToMessage.call({
      name: $name.val(),
      text: $text.val(),
      projectId: FlowRouter.getParam('_id'),
      messageId: this._id.substring(0,17),
    },(error) => {
      if(error)
        console.log(error);
    })

    $name.val('');
    $text.val('');


  }
});
