import './project-show.html';

import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
import { FlowRouter } from 'meteor/kadira:flow-router';
import { Projects } from '../../../api/projects/projects.js';




Template.Project_show.helpers({
  project(){
    return Projects.findOne(FlowRouter.getParam('_id'));
  },

});
