import './project-list.html';
import { Template } from 'meteor/templating';
import { Roles } from 'meteor/alanning:roles';
import { Projects } from '../../../api/projects/projects.js';
import { _ } from 'meteor/underscore';

Template.Project_list.helpers({
    Projects(){
      return Projects;
    },
    options(){
      const optionsArray = [];
      const users = Roles.getUsersInRole('admin');
      users.forEach((obj)=>{

        optionsArray.push({'label' : obj.emails[0].address, 'value' : obj._id})
      });
      return optionsArray;
    },
    yourProjects(){
      return Projects.find({})
    }
})
