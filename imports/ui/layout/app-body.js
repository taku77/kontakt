import './app-body.html';
import { Template } from 'meteor/templating';
import { Roles } from 'meteor/alanning:roles';
import { AccountsTemplates } from 'meteor/useraccounts:core';
import { updateRole } from '../../api/users/methods.js';
import { FlowRouter } from 'meteor/kadira:flow-router';
Template.App_body.onCreated(function appBodyOnCreate(){

  // updateRole.call({
  //   userId: Meteor.userId(),
  //   roles: ['admin']
  // })

  this.subscribe('messages.all');
  this.subscribe('users.showall');
  this.subscribe('projects.all');
})

Template.App_body.events({
  'click .logout-button'(e){
    e.preventDefault();
    if(Meteor.userId()){
        AccountsTemplates.logout();
        return FlowRouter.go('/signin');
    }



  }
})
